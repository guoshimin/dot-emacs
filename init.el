
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (dracula)))
 '(custom-safe-themes
   (quote
    ("b97a01622103266c1a26a032567e02d920b2c697ff69d40b7d9956821ab666cc" "dd6e52a5b1180f5c8bf408764a32867e2fa86594ded78a29040cafce6a4ea808" "ff7625ad8aa2615eae96d6b4469fcc7d3d20b2e1ebc63b761a349bebbb9d23cb" "d9129a8d924c4254607b5ded46350d68cc00b6e38c39fc137c3cfb7506702c12" "d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "a8245b7cc985a0610d71f9852e9f2767ad1b852c2bdea6f4aadc12cce9c4d6d0" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" default)))
 '(inhibit-startup-screen t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(powerline-default-theme)
(ac-config-default)
(projectile-global-mode)
(require 'cc-mode)
(require 'semantic)
(require 'ede)
(global-semanticdb-minor-mode 1)
(global-semantic-idle-scheduler-mode 1)
(global-semantic-stickyfunc-mode 1)

(semantic-mode 1)

;(defun alexott/cedet-hook ()
;  (local-set-key "\C-c\C-j" 'semantic-ia-fast-jump)
;  (local-set-key "\C-c\C-s" 'semantic-ia-show-summary))

;(add-hook 'c-mode-common-hook 'alexott/cedet-hook)
;(add-hook 'c-mode-hook 'alexott/cedet-hook)
;(add-hook 'c++-mode-hook 'alexott/cedet-hook)

(global-ede-mode)
(load-file "~/.emacs.d/rtags.el")

(add-hook 'prog-mode-hook
	  (lambda() (interactive)
	    (setq show-trailing-whitespace 1)))

(global-company-mode 1)
(helm-mode 1)
(load-file "~/.emacs.d/setup-helm.el")
(rtags-enable-standard-keybindings)

(add-hook 'before-save-hook 'delete-trailing-whitespace)
(add-hook 'python-mode-hook '(lambda () (elpy-mode 1)) t)
(global-auto-revert-mode 1)

(setq company-dabbrev-downcase nil)

(defadvice display-message-or-buffer (before ansi-color activate)
  "Process ANSI color codes in shell output."
  (let ((buf (ad-get-arg 0)))
    (and (bufferp buf)
         (string= (buffer-name buf) "*Shell Command Output*")
         (with-current-buffer buf
           (ansi-color-apply-on-region (point-min) (point-max))))))

(autoload 'ghc-init "ghc" nil t)
(autoload 'ghc-debug "ghc" nil t)
(add-hook 'haskell-mode-hook (lambda () (ghc-init)))
(add-hook 'haskell-mode-hook #'hindent-mode)
(custom-set-variables '(haskell-tags-on-save t))

(defvar jsonnet-mode-hook nil)
(add-to-list 'auto-mode-alist '("\\.jsonnet\\'" . jsonnet-mode))
(defun jsonnet-mode ()
  "Major mode for editing Workflow Process Description Language files"
  (interactive)
  (kill-all-local-variables)
  ;(set-syntax-table wpdl-mode-syntax-table)
  ;(use-local-map wpdl-mode-map)
  (setq major-mode 'jsonnet-mode)
  (setq mode-name "JSONNET")
  (run-hooks 'jsonnet-mode-hook))

(defun jsonnet-fmt-call-bin (input-buffer output-buffer start-line end-line)
  "Call process jsonnet fmt on INPUT-BUFFER saving the output to OUTPUT-BUFFER.
Return the exit code."
  (with-current-buffer input-buffer
    (call-process-region (point-min) (point-max) "jsonnet" nil output-buffer nil "fmt" "-n" "4" "-")))

(defun jsonnet-eval-call-bin (input-buffer output-buffer start-line end-line)
  "Call process jsonnet fmt on INPUT-BUFFER saving the output to OUTPUT-BUFFER.
Return the exit code."
  (with-current-buffer input-buffer
    (call-process-region (point-min) (point-max) "jsonnet" nil output-buffer nil "eval" "-")))

(defun jsonnet-eval-yaml-call-bin (input-buffer output-buffer start-line end-line)
  "Call process jsonnet fmt on INPUT-BUFFER saving the output to OUTPUT-BUFFER.
Return the exit code."
  (with-current-buffer input-buffer
    (call-process-region (point-min) (point-max) "bash" nil output-buffer nil "-lc" "jsonnet eval - | python /home/shimin/analytics/kube/cmd/json2yaml")))

;;;###autoload
(defun jsonnet-fmt-buffer (beginning end)
  "Try to jsonnet fmt the current buffer.
If jsonnet exits with an error, the output will be shown in a help-window."
  (interactive "r")
  (let* ((original-buffer (current-buffer))
         (original-point (point))  ; Because we are replacing text, save-excursion does not always work.
         (original-window-pos (window-start))
         (start-line (line-number-at-pos (if (use-region-p)
                                             beginning
                                           (point-min))))
         (end-line (line-number-at-pos (if (use-region-p)
                                           (if (or (= (char-before end) 10) (= (char-before end) 13))
                                               (- end 1)
                                             end)
                                         (point-max))))
         (tmpbuf (generate-new-buffer "*jsonnet-fmt*"))
         (exit-code (jsonnet-fmt-call-bin original-buffer tmpbuf start-line end-line)))

    (deactivate-mark)
    ;; There are three exit-codes defined for YAPF:
    ;; 0: Exit with success (change or no change on yapf >=0.11)
    ;; 1: Exit with error
    ;; 2: Exit with success and change (Backward compatibility)
    ;; anything else would be very unexpected.
    (cond ((or (eq exit-code 0) (eq exit-code 2))
           (with-current-buffer tmpbuf
             (copy-to-buffer original-buffer (point-min) (point-max))))
          ((eq exit-code 1)
           (error "jsonnet fmt failed, see *jsonnet-fmt* buffer for details")))

    ;; Clean up tmpbuf
    (kill-buffer tmpbuf)
    ;; restore window to similar state
    (goto-char original-point)
    (set-window-start (selected-window) original-window-pos)))

(defun jsonnet-eval-buffer (beginning end)
  "Try to jsonnet fmt the current buffer.
If jsonnet exits with an error, the output will be shown in a help-window."
  (interactive "r")
  (let* ((original-buffer (current-buffer))
         (original-point (point))  ; Because we are replacing text, save-excursion does not always work.
         (original-window-pos (window-start))
         (start-line (line-number-at-pos (if (use-region-p)
                                             beginning
                                           (point-min))))
         (end-line (line-number-at-pos (if (use-region-p)
                                           (if (or (= (char-before end) 10) (= (char-before end) 13))
                                               (- end 1)
                                             end)
                                         (point-max))))
         (tmpbuf (get-buffer-create "*jsonnet-eval*")))

    (with-current-buffer tmpbuf
      (erase-buffer))
    (jsonnet-eval-yaml-call-bin original-buffer tmpbuf start-line end-line)
    (deactivate-mark)
    (display-buffer tmpbuf)
    (goto-char original-point)
    (set-window-start (selected-window) original-window-pos)))



(defun jsonnet-fmt-non-interactive ()
  "Wrapper to allow calling yapfify-buffer non-interactively. ie. in hooks etc."
  (call-interactively 'jsonnet-fmt-buffer))

(defun my-jsonnet-hook ()
  (add-hook 'before-save-hook 'jsonnet-fmt-non-interactive))

(add-hook 'jsonnet-mode-hook 'my-jsonnet-hook)

(defun desperately-compile ()
  "Traveling up the path, find a Makefile and `compile'."
  (interactive)
  (when (locate-dominating-file default-directory "Makefile")
  (with-temp-buffer
    (cd (locate-dominating-file default-directory "Makefile"))
    (compile "make -k -j8"))))
